﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Mvc;
using System.Web.Mvc;
using System.Net.Mail;
using PPOC.Models;
using System.Net;

namespace PPOC.Controllers
{
    public class SellerRegistrationSurfaceController : SurfaceController
    {

        public const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Membership/";

        public ActionResult RenderForm()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_SellerRegistrationForm.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(SellerRegistrationModel model)
        {
            //if (!ModelState.IsValid)
            //{





            //    //SendEmail(model);

            //    //return RedirectToCurrentUmbracoPage();
            //}

            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            var memberService = Services.MemberService;

            if (memberService.GetByEmail(model.EmailAddress) != null)
            {
                ModelState.AddModelError("", "That e-mail address is already taken.");
                return CurrentUmbracoPage();
            }

            var member = memberService.CreateMember(model.EmailAddress, model.EmailAddress, string.Format("{0} {1}", model.FirstName, model.LastName), "Seller");
            //member.SetValue("IsActivated", model.IsActivated);
            member.IsApproved = false;

            memberService.Save(member);
            SendEmail(model);

            return CurrentUmbracoPage();
        }


        private void SendEmail(SellerRegistrationModel model)
        {

            MailMessage message = new MailMessage("justin.ravago@redcoresolutions.com", model.EmailAddress);
            message.Subject = string.Format("New Seller Registration: {0} {1} - {2}", model.FirstName, model.LastName, model.EmailAddress);
            message.Body = "<div style='border:3px solid #eee;border-radius:10px;font-family:Segoe UI;padding:20px;'>" +
                "<div style='background:#2790B0; width:100%;padding-top:10px; height:50px;text-align:center;'><img src='http://raddnet.com/review/project/image/logo_cms.png'></div><hr/>" + 
                "You have received this message because you registered as a seller from Juts' localhost website. <br/><br/>" +
                "To activate your account, please click here: <a href='google.com.ph' target='_blank'>" + MakeRandomString(20) + 
                "</a> <br/><br/><br/>" +
                "Sincerely, <br /> <h3>PPOC ni Juts</h3></hr></div>";
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("justin.ravago@redcoresolutions.com", "");
            client.Send(message);
        }

        // super code generator
        private string MakeRandomString(int size)
        {
            Random rand = new Random();
            // Characters we will use to generate this random string.
            char[] allowableChars = "ABCDEFGHIJKLOMNOPQRSTUVWXYZ0123456789".ToCharArray();

            // Start generating the random string.
            string activationCode = string.Empty;
            for (int i = 0; i <= size - 1; i++)
            {
                activationCode += allowableChars[rand.Next(allowableChars.Length - 1)];
            }

            // Return the random string in upper case.
            return activationCode.ToUpper();
        }


    }
}