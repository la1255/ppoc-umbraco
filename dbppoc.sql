CREATE DATABASE  IF NOT EXISTS `dbppoc` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dbppoc`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dbppoc
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cmscontent`
--

DROP TABLE IF EXISTS `cmscontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmscontent` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `nodeId` int(11) NOT NULL,
  `contentType` int(11) NOT NULL,
  PRIMARY KEY (`pk`),
  KEY `IX_cmsContent` (`nodeId`),
  KEY `contentType` (`contentType`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmscontent`
--

LOCK TABLES `cmscontent` WRITE;
/*!40000 ALTER TABLE `cmscontent` DISABLE KEYS */;
INSERT INTO `cmscontent` VALUES (1,1055,1052),(2,1056,1054),(12,1067,1057),(13,1068,1057),(16,1071,1057),(15,1070,1057),(14,1069,1057);
/*!40000 ALTER TABLE `cmscontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmscontenttype`
--

DROP TABLE IF EXISTS `cmscontenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmscontenttype` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `nodeId` int(11) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'folder.png',
  `description` varchar(1500) CHARACTER SET utf8 DEFAULT NULL,
  `isContainer` tinyint(1) NOT NULL DEFAULT '0',
  `allowAtRoot` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk`),
  KEY `IX_cmsContentType` (`nodeId`),
  KEY `IX_cmsContentType_icon` (`icon`)
) ENGINE=MyISAM AUTO_INCREMENT=538 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmscontenttype`
--

LOCK TABLES `cmscontenttype` WRITE;
/*!40000 ALTER TABLE `cmscontenttype` DISABLE KEYS */;
INSERT INTO `cmscontenttype` VALUES (532,1031,'Folder','icon-folder','icon-folder',NULL,0,1),(533,1032,'Image','icon-picture','icon-picture',NULL,0,1),(534,1033,'File','icon-document','icon-document',NULL,0,1),(531,1044,'Member','icon-user','icon-user',NULL,0,0),(535,1052,'master','icon-home','folder.png',NULL,0,1),(536,1054,'sellerRegistration','icon-users-alt','folder.png',NULL,0,0),(537,1057,'seller','icon-user','folder.png',NULL,0,0);
/*!40000 ALTER TABLE `cmscontenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmscontenttype2contenttype`
--

DROP TABLE IF EXISTS `cmscontenttype2contenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmscontenttype2contenttype` (
  `parentContentTypeId` int(11) NOT NULL,
  `childContentTypeId` int(11) NOT NULL,
  PRIMARY KEY (`parentContentTypeId`,`childContentTypeId`),
  KEY `childContentTypeId` (`childContentTypeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmscontenttype2contenttype`
--

LOCK TABLES `cmscontenttype2contenttype` WRITE;
/*!40000 ALTER TABLE `cmscontenttype2contenttype` DISABLE KEYS */;
/*!40000 ALTER TABLE `cmscontenttype2contenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmscontenttypeallowedcontenttype`
--

DROP TABLE IF EXISTS `cmscontenttypeallowedcontenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmscontenttypeallowedcontenttype` (
  `Id` int(11) NOT NULL,
  `AllowedId` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`,`AllowedId`),
  KEY `AllowedId` (`AllowedId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmscontenttypeallowedcontenttype`
--

LOCK TABLES `cmscontenttypeallowedcontenttype` WRITE;
/*!40000 ALTER TABLE `cmscontenttypeallowedcontenttype` DISABLE KEYS */;
INSERT INTO `cmscontenttypeallowedcontenttype` VALUES (1031,1031,0),(1031,1032,0),(1031,1033,0),(1052,1054,0);
/*!40000 ALTER TABLE `cmscontenttypeallowedcontenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmscontentversion`
--

DROP TABLE IF EXISTS `cmscontentversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmscontentversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ContentId` int(11) NOT NULL,
  `VersionId` char(36) NOT NULL,
  `VersionDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IX_cmsContentVersion_ContentId` (`ContentId`),
  KEY `IX_cmsContentVersion_VersionId` (`VersionId`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmscontentversion`
--

LOCK TABLES `cmscontentversion` WRITE;
/*!40000 ALTER TABLE `cmscontentversion` DISABLE KEYS */;
INSERT INTO `cmscontentversion` VALUES (1,1055,'3a9aa966-4596-442a-a8cd-9ae29b2b2e72','2017-09-05 14:28:26'),(2,1056,'a491134f-3eb0-4b54-85d6-464636c76fd4','2017-09-05 14:29:06'),(12,1067,'e091ba58-35fc-4bca-b49a-4a43f6e4cb76','2017-09-05 15:23:09'),(13,1068,'da58552f-b103-4ae1-ba3d-6f7a83535f15','2017-09-05 15:23:48'),(16,1071,'52fd3723-dd3f-43db-8506-be1bcc5603a7','2017-09-05 15:24:32'),(15,1070,'3735ce65-9e7a-4f15-9816-20ea334cdae5','2017-09-05 15:24:18'),(14,1069,'7c401afa-53c4-4543-a402-7a95a1cfade2','2017-09-05 15:24:05');
/*!40000 ALTER TABLE `cmscontentversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmscontentxml`
--

DROP TABLE IF EXISTS `cmscontentxml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmscontentxml` (
  `nodeId` int(11) NOT NULL,
  `xml` longtext NOT NULL,
  PRIMARY KEY (`nodeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmscontentxml`
--

LOCK TABLES `cmscontentxml` WRITE;
/*!40000 ALTER TABLE `cmscontentxml` DISABLE KEYS */;
INSERT INTO `cmscontentxml` VALUES (1055,'<master id=\"1055\" key=\"c3123efd-8013-403e-b413-a2ae2d4a48a1\" parentID=\"-1\" level=\"1\" creatorID=\"0\" sortOrder=\"0\" createDate=\"2017-09-05T22:28:26\" updateDate=\"2017-09-05T22:28:26\" nodeName=\"Homepage\" urlName=\"homepage\" path=\"-1,1055\" isDoc=\"\" nodeType=\"1052\" creatorName=\"Justin R\" writerName=\"Justin R\" writerID=\"0\" template=\"1051\" nodeTypeAlias=\"master\"><websiteTitle><![CDATA[Home Page]]></websiteTitle></master>'),(1056,'<sellerRegistration id=\"1056\" key=\"9b129fca-3d25-44b0-a828-f905a815d77c\" parentID=\"1055\" level=\"2\" creatorID=\"0\" sortOrder=\"0\" createDate=\"2017-09-05T22:29:05\" updateDate=\"2017-09-05T22:29:05\" nodeName=\"Register as Seller\" urlName=\"register-as-seller\" path=\"-1,1055,1056\" isDoc=\"\" nodeType=\"1054\" creatorName=\"Justin R\" writerName=\"Justin R\" writerID=\"0\" template=\"1053\" nodeTypeAlias=\"sellerRegistration\" />'),(1067,'<seller id=\"1067\" key=\"38d63467-bfa9-44a9-a02b-28de033378e5\" parentID=\"-1\" level=\"1\" creatorID=\"0\" sortOrder=\"0\" createDate=\"2017-09-05T23:23:08\" updateDate=\"2017-09-05T23:23:08\" nodeName=\"Justin Ravago\" urlName=\"justin-ravago\" path=\"-1,1067\" isDoc=\"\" nodeType=\"1057\" nodeTypeAlias=\"seller\" loginName=\"justin.ravago@redcoresolutions.com\" email=\"justin.ravago@redcoresolutions.com\" icon=\"icon-user\"><umbracoMemberApproved>0</umbracoMemberApproved></seller>'),(1068,'<seller id=\"1068\" key=\"13cdf5e1-5eb2-424a-b2c5-f4afd927aa2f\" parentID=\"-1\" level=\"1\" creatorID=\"0\" sortOrder=\"1\" createDate=\"2017-09-05T23:23:48\" updateDate=\"2017-09-05T23:23:48\" nodeName=\"Frank Claveria\" urlName=\"frank-claveria\" path=\"-1,1068\" isDoc=\"\" nodeType=\"1057\" nodeTypeAlias=\"seller\" loginName=\"frank.claveria@redcoresolutions.com\" email=\"frank.claveria@redcoresolutions.com\" icon=\"icon-user\"><umbracoMemberApproved>0</umbracoMemberApproved></seller>'),(1069,'<seller id=\"1069\" key=\"4eae977e-033e-4d53-9e09-b1d60d576e1e\" parentID=\"-1\" level=\"1\" creatorID=\"0\" sortOrder=\"2\" createDate=\"2017-09-05T23:24:04\" updateDate=\"2017-09-05T23:24:04\" nodeName=\"Duane Gavino\" urlName=\"duane-gavino\" path=\"-1,1069\" isDoc=\"\" nodeType=\"1057\" nodeTypeAlias=\"seller\" loginName=\"duane.gavino@redcoresolutions.com\" email=\"duane.gavino@redcoresolutions.com\" icon=\"icon-user\"><umbracoMemberApproved>0</umbracoMemberApproved></seller>'),(1070,'<seller id=\"1070\" key=\"5e2cc6d3-99d3-4f60-8f10-bea17f5a8d40\" parentID=\"-1\" level=\"1\" creatorID=\"0\" sortOrder=\"3\" createDate=\"2017-09-05T23:24:17\" updateDate=\"2017-09-05T23:24:17\" nodeName=\"Danilo Buhain\" urlName=\"danilo-buhain\" path=\"-1,1070\" isDoc=\"\" nodeType=\"1057\" nodeTypeAlias=\"seller\" loginName=\"danilo.buhain@redcoresolutions.com\" email=\"danilo.buhain@redcoresolutions.com\" icon=\"icon-user\"><umbracoMemberApproved>0</umbracoMemberApproved></seller>'),(1071,'<seller id=\"1071\" key=\"3b8654b5-df96-47e4-8384-dfdddd4d4d5b\" parentID=\"-1\" level=\"1\" creatorID=\"0\" sortOrder=\"4\" createDate=\"2017-09-05T23:24:31\" updateDate=\"2017-09-05T23:24:31\" nodeName=\"Christian Bailon\" urlName=\"christian-bailon\" path=\"-1,1071\" isDoc=\"\" nodeType=\"1057\" nodeTypeAlias=\"seller\" loginName=\"christian.bailon@redcoresolutions.com\" email=\"christian.bailon@redcoresolutions.com\" icon=\"icon-user\"><umbracoMemberApproved>0</umbracoMemberApproved></seller>');
/*!40000 ALTER TABLE `cmscontentxml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmsdatatype`
--

DROP TABLE IF EXISTS `cmsdatatype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmsdatatype` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `nodeId` int(11) NOT NULL,
  `propertyEditorAlias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `dbType` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`pk`),
  KEY `IX_cmsDataType_nodeId` (`nodeId`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmsdatatype`
--

LOCK TABLES `cmsdatatype` WRITE;
/*!40000 ALTER TABLE `cmsdatatype` DISABLE KEYS */;
INSERT INTO `cmsdatatype` VALUES (1,-49,'Umbraco.TrueFalse','Integer'),(2,-51,'Umbraco.Integer','Integer'),(3,-87,'Umbraco.TinyMCEv3','Ntext'),(4,-88,'Umbraco.Textbox','Nvarchar'),(5,-89,'Umbraco.TextboxMultiple','Ntext'),(6,-90,'Umbraco.UploadField','Nvarchar'),(7,-92,'Umbraco.NoEdit','Nvarchar'),(8,-36,'Umbraco.DateTime','Date'),(9,-37,'Umbraco.ColorPickerAlias','Nvarchar'),(11,-39,'Umbraco.DropDownMultiple','Nvarchar'),(12,-40,'Umbraco.RadioButtonList','Nvarchar'),(13,-41,'Umbraco.Date','Date'),(14,-42,'Umbraco.DropDown','Integer'),(15,-43,'Umbraco.CheckBoxList','Nvarchar'),(22,1041,'Umbraco.Tags','Ntext'),(24,1043,'Umbraco.ImageCropper','Ntext'),(-26,-95,'Umbraco.ListView','Nvarchar'),(-27,-96,'Umbraco.ListView','Nvarchar'),(-28,-97,'Umbraco.ListView','Nvarchar'),(26,1046,'Umbraco.ContentPicker2','Nvarchar'),(27,1047,'Umbraco.MemberPicker2','Nvarchar'),(28,1048,'Umbraco.MediaPicker2','Ntext'),(29,1049,'Umbraco.MediaPicker2','Ntext'),(30,1050,'Umbraco.RelatedLinks2','Ntext');
/*!40000 ALTER TABLE `cmsdatatype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmsdatatypeprevalues`
--

DROP TABLE IF EXISTS `cmsdatatypeprevalues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmsdatatypeprevalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datatypeNodeId` int(11) NOT NULL,
  `value` longtext,
  `sortorder` int(11) NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `datatypeNodeId` (`datatypeNodeId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmsdatatypeprevalues`
--

LOCK TABLES `cmsdatatypeprevalues` WRITE;
/*!40000 ALTER TABLE `cmsdatatypeprevalues` DISABLE KEYS */;
INSERT INTO `cmsdatatypeprevalues` VALUES (3,-87,',code,undo,redo,cut,copy,mcepasteword,stylepicker,bold,italic,bullist,numlist,outdent,indent,mcelink,unlink,mceinsertanchor,mceimage,umbracomacro,mceinserttable,umbracoembed,mcecharmap,|1|1,2,3,|0|500,400|1049,|true|',0,''),(4,1041,'default',0,'group'),(-1,-97,'10',1,'pageSize'),(-2,-97,'username',2,'orderBy'),(-3,-97,'asc',3,'orderDirection'),(-4,-97,'[{\"alias\":\"username\",\"isSystem\":1},{\"alias\":\"email\",\"isSystem\":1},{\"alias\":\"updateDate\",\"header\":\"Last edited\",\"isSystem\":1}]',4,'includeProperties'),(-5,-96,'100',1,'pageSize'),(-6,-96,'updateDate',2,'orderBy'),(-7,-96,'desc',3,'orderDirection'),(-8,-96,'[{\"name\": \"Grid\",\"path\": \"views/propertyeditors/listview/layouts/grid/grid.html\", \"icon\": \"icon-thumbnails-small\", \"isSystem\": 1, \"selected\": true},{\"name\": \"List\",\"path\": \"views/propertyeditors/listview/layouts/list/list.html\",\"icon\": \"icon-list\", \"isSystem\": 1,\"selected\": true}]',4,'layouts'),(-9,-96,'[{\"alias\":\"updateDate\",\"header\":\"Last edited\",\"isSystem\":1},{\"alias\":\"owner\",\"header\":\"Updated by\",\"isSystem\":1}]',5,'includeProperties'),(6,1049,'1',0,'multiPicker');
/*!40000 ALTER TABLE `cmsdatatypeprevalues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmsdictionary`
--

DROP TABLE IF EXISTS `cmsdictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmsdictionary` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `id` char(36) NOT NULL,
  `parent` char(36) DEFAULT NULL,
  `key` varchar(1000) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`pk`),
  KEY `IX_cmsDictionary_id` (`id`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmsdictionary`
--

LOCK TABLES `cmsdictionary` WRITE;
/*!40000 ALTER TABLE `cmsdictionary` DISABLE KEYS */;
/*!40000 ALTER TABLE `cmsdictionary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmsdocument`
--

DROP TABLE IF EXISTS `cmsdocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmsdocument` (
  `nodeId` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `documentUser` int(11) NOT NULL,
  `versionId` char(36) NOT NULL,
  `text` varchar(255) CHARACTER SET utf8 NOT NULL,
  `releaseDate` timestamp NULL DEFAULT NULL,
  `expireDate` timestamp NULL DEFAULT NULL,
  `updateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `templateId` int(11) DEFAULT NULL,
  `newest` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`versionId`),
  KEY `IX_cmsDocument` (`nodeId`,`versionId`),
  KEY `IX_cmsDocument_published` (`published`),
  KEY `IX_cmsDocument_newest` (`newest`),
  KEY `templateId` (`templateId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmsdocument`
--

LOCK TABLES `cmsdocument` WRITE;
/*!40000 ALTER TABLE `cmsdocument` DISABLE KEYS */;
INSERT INTO `cmsdocument` VALUES (1055,1,0,'3a9aa966-4596-442a-a8cd-9ae29b2b2e72','Homepage',NULL,NULL,'2017-09-05 14:28:26',1051,1),(1056,1,0,'a491134f-3eb0-4b54-85d6-464636c76fd4','Register as Seller',NULL,NULL,'2017-09-05 14:29:06',1053,1);
/*!40000 ALTER TABLE `cmsdocument` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmsdocumenttype`
--

DROP TABLE IF EXISTS `cmsdocumenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmsdocumenttype` (
  `contentTypeNodeId` int(11) NOT NULL,
  `templateNodeId` int(11) NOT NULL,
  `IsDefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contentTypeNodeId`,`templateNodeId`),
  KEY `templateNodeId` (`templateNodeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmsdocumenttype`
--

LOCK TABLES `cmsdocumenttype` WRITE;
/*!40000 ALTER TABLE `cmsdocumenttype` DISABLE KEYS */;
INSERT INTO `cmsdocumenttype` VALUES (1052,1051,1),(1054,1053,1);
/*!40000 ALTER TABLE `cmsdocumenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmslanguagetext`
--

DROP TABLE IF EXISTS `cmslanguagetext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmslanguagetext` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `languageId` int(11) NOT NULL,
  `UniqueId` char(36) NOT NULL,
  `value` varchar(1000) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`pk`),
  KEY `languageId` (`languageId`),
  KEY `UniqueId` (`UniqueId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmslanguagetext`
--

LOCK TABLES `cmslanguagetext` WRITE;
/*!40000 ALTER TABLE `cmslanguagetext` DISABLE KEYS */;
/*!40000 ALTER TABLE `cmslanguagetext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmsmacro`
--

DROP TABLE IF EXISTS `cmsmacro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmsmacro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqueId` char(36) NOT NULL,
  `macroUseInEditor` tinyint(1) NOT NULL DEFAULT '0',
  `macroRefreshRate` int(11) NOT NULL DEFAULT '0',
  `macroAlias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `macroName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `macroScriptType` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `macroScriptAssembly` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `macroXSLT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `macroCacheByPage` tinyint(1) NOT NULL DEFAULT '1',
  `macroCachePersonalized` tinyint(1) NOT NULL DEFAULT '0',
  `macroDontRender` tinyint(1) NOT NULL DEFAULT '0',
  `macroPython` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_cmsMacro_UniqueId` (`uniqueId`),
  KEY `IX_cmsMacroPropertyAlias` (`macroAlias`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmsmacro`
--

LOCK TABLES `cmsmacro` WRITE;
/*!40000 ALTER TABLE `cmsmacro` DISABLE KEYS */;
/*!40000 ALTER TABLE `cmsmacro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmsmacroproperty`
--

DROP TABLE IF EXISTS `cmsmacroproperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmsmacroproperty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniquePropertyId` char(36) NOT NULL,
  `editorAlias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `macro` int(11) NOT NULL,
  `macroPropertySortOrder` int(11) NOT NULL DEFAULT '0',
  `macroPropertyAlias` varchar(50) CHARACTER SET utf8 NOT NULL,
  `macroPropertyName` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_cmsMacroProperty_UniquePropertyId` (`uniquePropertyId`),
  KEY `IX_cmsMacroProperty_Alias` (`macro`,`macroPropertyAlias`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmsmacroproperty`
--

LOCK TABLES `cmsmacroproperty` WRITE;
/*!40000 ALTER TABLE `cmsmacroproperty` DISABLE KEYS */;
/*!40000 ALTER TABLE `cmsmacroproperty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmsmember`
--

DROP TABLE IF EXISTS `cmsmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmsmember` (
  `nodeId` int(11) NOT NULL,
  `Email` varchar(1000) CHARACTER SET utf8 NOT NULL DEFAULT '''',
  `LoginName` varchar(1000) CHARACTER SET utf8 NOT NULL DEFAULT '''',
  `Password` varchar(1000) CHARACTER SET utf8 NOT NULL DEFAULT '''',
  PRIMARY KEY (`nodeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmsmember`
--

LOCK TABLES `cmsmember` WRITE;
/*!40000 ALTER TABLE `cmsmember` DISABLE KEYS */;
INSERT INTO `cmsmember` VALUES (1067,'justin.ravago@redcoresolutions.com','justin.ravago@redcoresolutions.com',''),(1068,'frank.claveria@redcoresolutions.com','frank.claveria@redcoresolutions.com',''),(1069,'duane.gavino@redcoresolutions.com','duane.gavino@redcoresolutions.com',''),(1070,'danilo.buhain@redcoresolutions.com','danilo.buhain@redcoresolutions.com',''),(1071,'christian.bailon@redcoresolutions.com','christian.bailon@redcoresolutions.com','');
/*!40000 ALTER TABLE `cmsmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmsmember2membergroup`
--

DROP TABLE IF EXISTS `cmsmember2membergroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmsmember2membergroup` (
  `Member` int(11) NOT NULL,
  `MemberGroup` int(11) NOT NULL,
  PRIMARY KEY (`Member`,`MemberGroup`),
  KEY `MemberGroup` (`MemberGroup`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmsmember2membergroup`
--

LOCK TABLES `cmsmember2membergroup` WRITE;
/*!40000 ALTER TABLE `cmsmember2membergroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `cmsmember2membergroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmsmembertype`
--

DROP TABLE IF EXISTS `cmsmembertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmsmembertype` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `NodeId` int(11) NOT NULL,
  `propertytypeId` int(11) NOT NULL,
  `memberCanEdit` tinyint(1) NOT NULL DEFAULT '0',
  `viewOnProfile` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk`),
  KEY `NodeId` (`NodeId`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmsmembertype`
--

LOCK TABLES `cmsmembertype` WRITE;
/*!40000 ALTER TABLE `cmsmembertype` DISABLE KEYS */;
INSERT INTO `cmsmembertype` VALUES (1,1044,35,0,0),(2,1044,36,0,0),(3,1044,32,0,0),(4,1044,33,0,0),(5,1044,28,0,0),(6,1044,34,0,0),(7,1044,29,0,0),(8,1044,30,0,0),(9,1044,31,0,0),(10,1057,38,0,0),(11,1057,39,0,0),(12,1057,40,0,0),(13,1057,41,0,0),(14,1057,42,0,0),(15,1057,43,0,0),(16,1057,44,0,0),(17,1057,45,0,0),(18,1057,46,0,0);
/*!40000 ALTER TABLE `cmsmembertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmspreviewxml`
--

DROP TABLE IF EXISTS `cmspreviewxml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmspreviewxml` (
  `nodeId` int(11) NOT NULL,
  `versionId` char(36) NOT NULL,
  `timestamp` timestamp NOT NULL,
  `xml` longtext NOT NULL,
  PRIMARY KEY (`nodeId`,`versionId`),
  KEY `versionId` (`versionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmspreviewxml`
--

LOCK TABLES `cmspreviewxml` WRITE;
/*!40000 ALTER TABLE `cmspreviewxml` DISABLE KEYS */;
INSERT INTO `cmspreviewxml` VALUES (1055,'3a9aa966-4596-442a-a8cd-9ae29b2b2e72','2017-09-05 14:28:26','<master id=\"1055\" key=\"c3123efd-8013-403e-b413-a2ae2d4a48a1\" parentID=\"-1\" level=\"1\" creatorID=\"0\" sortOrder=\"0\" createDate=\"2017-09-05T22:28:26\" updateDate=\"2017-09-05T22:28:26\" nodeName=\"Homepage\" urlName=\"homepage\" path=\"-1,1055\" isDoc=\"\" nodeType=\"1052\" creatorName=\"Justin R\" writerName=\"Justin R\" writerID=\"0\" template=\"1051\" nodeTypeAlias=\"master\"><websiteTitle><![CDATA[Home Page]]></websiteTitle></master>'),(1056,'a491134f-3eb0-4b54-85d6-464636c76fd4','2017-09-05 14:29:06','<sellerRegistration id=\"1056\" key=\"9b129fca-3d25-44b0-a828-f905a815d77c\" parentID=\"1055\" level=\"2\" creatorID=\"0\" sortOrder=\"0\" createDate=\"2017-09-05T22:29:05\" updateDate=\"2017-09-05T22:29:05\" nodeName=\"Register as Seller\" urlName=\"register-as-seller\" path=\"-1,1055,1056\" isDoc=\"\" nodeType=\"1054\" creatorName=\"Justin R\" writerName=\"Justin R\" writerID=\"0\" template=\"1053\" nodeTypeAlias=\"sellerRegistration\" />');
/*!40000 ALTER TABLE `cmspreviewxml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmspropertydata`
--

DROP TABLE IF EXISTS `cmspropertydata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmspropertydata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentNodeId` int(11) NOT NULL,
  `versionId` char(36) DEFAULT NULL,
  `propertytypeid` int(11) NOT NULL,
  `dataInt` int(11) DEFAULT NULL,
  `dataDecimal` decimal(38,6) DEFAULT NULL,
  `dataDate` timestamp NULL DEFAULT NULL,
  `dataNvarchar` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `dataNtext` longtext,
  PRIMARY KEY (`id`),
  KEY `IX_cmsPropertyData_1` (`contentNodeId`,`versionId`,`propertytypeid`),
  KEY `IX_cmsPropertyData_2` (`versionId`),
  KEY `IX_cmsPropertyData_3` (`propertytypeid`)
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmspropertydata`
--

LOCK TABLES `cmspropertydata` WRITE;
/*!40000 ALTER TABLE `cmspropertydata` DISABLE KEYS */;
INSERT INTO `cmspropertydata` VALUES (1,1055,'3a9aa966-4596-442a-a8cd-9ae29b2b2e72',37,NULL,NULL,NULL,'Home Page',NULL),(122,1071,'52fd3723-dd3f-43db-8506-be1bcc5603a7',38,NULL,NULL,NULL,NULL,NULL),(121,1071,'52fd3723-dd3f-43db-8506-be1bcc5603a7',42,NULL,NULL,NULL,NULL,NULL),(120,1071,'52fd3723-dd3f-43db-8506-be1bcc5603a7',46,NULL,NULL,NULL,NULL,NULL),(119,1071,'52fd3723-dd3f-43db-8506-be1bcc5603a7',41,0,NULL,NULL,NULL,NULL),(118,1070,'3735ce65-9e7a-4f15-9816-20ea334cdae5',45,NULL,NULL,NULL,NULL,NULL),(117,1070,'3735ce65-9e7a-4f15-9816-20ea334cdae5',40,0,NULL,NULL,NULL,NULL),(116,1070,'3735ce65-9e7a-4f15-9816-20ea334cdae5',44,NULL,NULL,NULL,NULL,NULL),(115,1070,'3735ce65-9e7a-4f15-9816-20ea334cdae5',39,NULL,NULL,NULL,NULL,NULL),(114,1070,'3735ce65-9e7a-4f15-9816-20ea334cdae5',43,NULL,NULL,NULL,NULL,NULL),(113,1070,'3735ce65-9e7a-4f15-9816-20ea334cdae5',38,NULL,NULL,NULL,NULL,NULL),(112,1070,'3735ce65-9e7a-4f15-9816-20ea334cdae5',42,NULL,NULL,NULL,NULL,NULL),(111,1070,'3735ce65-9e7a-4f15-9816-20ea334cdae5',46,NULL,NULL,NULL,NULL,NULL),(110,1070,'3735ce65-9e7a-4f15-9816-20ea334cdae5',41,0,NULL,NULL,NULL,NULL),(109,1069,'7c401afa-53c4-4543-a402-7a95a1cfade2',45,NULL,NULL,NULL,NULL,NULL),(108,1069,'7c401afa-53c4-4543-a402-7a95a1cfade2',40,0,NULL,NULL,NULL,NULL),(107,1069,'7c401afa-53c4-4543-a402-7a95a1cfade2',44,NULL,NULL,NULL,NULL,NULL),(106,1069,'7c401afa-53c4-4543-a402-7a95a1cfade2',39,NULL,NULL,NULL,NULL,NULL),(105,1069,'7c401afa-53c4-4543-a402-7a95a1cfade2',43,NULL,NULL,NULL,NULL,NULL),(104,1069,'7c401afa-53c4-4543-a402-7a95a1cfade2',38,NULL,NULL,NULL,NULL,NULL),(103,1069,'7c401afa-53c4-4543-a402-7a95a1cfade2',42,NULL,NULL,NULL,NULL,NULL),(102,1069,'7c401afa-53c4-4543-a402-7a95a1cfade2',46,NULL,NULL,NULL,NULL,NULL),(101,1069,'7c401afa-53c4-4543-a402-7a95a1cfade2',41,0,NULL,NULL,NULL,NULL),(100,1068,'da58552f-b103-4ae1-ba3d-6f7a83535f15',45,NULL,NULL,NULL,NULL,NULL),(99,1068,'da58552f-b103-4ae1-ba3d-6f7a83535f15',40,0,NULL,NULL,NULL,NULL),(98,1068,'da58552f-b103-4ae1-ba3d-6f7a83535f15',44,NULL,NULL,NULL,NULL,NULL),(97,1068,'da58552f-b103-4ae1-ba3d-6f7a83535f15',39,NULL,NULL,NULL,NULL,NULL),(83,1067,'e091ba58-35fc-4bca-b49a-4a43f6e4cb76',41,0,NULL,NULL,NULL,NULL),(84,1067,'e091ba58-35fc-4bca-b49a-4a43f6e4cb76',46,NULL,NULL,NULL,NULL,NULL),(85,1067,'e091ba58-35fc-4bca-b49a-4a43f6e4cb76',42,NULL,NULL,NULL,NULL,NULL),(86,1067,'e091ba58-35fc-4bca-b49a-4a43f6e4cb76',38,NULL,NULL,NULL,NULL,NULL),(87,1067,'e091ba58-35fc-4bca-b49a-4a43f6e4cb76',43,NULL,NULL,NULL,NULL,NULL),(88,1067,'e091ba58-35fc-4bca-b49a-4a43f6e4cb76',39,NULL,NULL,NULL,NULL,NULL),(89,1067,'e091ba58-35fc-4bca-b49a-4a43f6e4cb76',44,NULL,NULL,NULL,NULL,NULL),(90,1067,'e091ba58-35fc-4bca-b49a-4a43f6e4cb76',40,0,NULL,NULL,NULL,NULL),(91,1067,'e091ba58-35fc-4bca-b49a-4a43f6e4cb76',45,NULL,NULL,NULL,NULL,NULL),(92,1068,'da58552f-b103-4ae1-ba3d-6f7a83535f15',41,0,NULL,NULL,NULL,NULL),(93,1068,'da58552f-b103-4ae1-ba3d-6f7a83535f15',46,NULL,NULL,NULL,NULL,NULL),(94,1068,'da58552f-b103-4ae1-ba3d-6f7a83535f15',42,NULL,NULL,NULL,NULL,NULL),(95,1068,'da58552f-b103-4ae1-ba3d-6f7a83535f15',38,NULL,NULL,NULL,NULL,NULL),(96,1068,'da58552f-b103-4ae1-ba3d-6f7a83535f15',43,NULL,NULL,NULL,NULL,NULL),(123,1071,'52fd3723-dd3f-43db-8506-be1bcc5603a7',43,NULL,NULL,NULL,NULL,NULL),(124,1071,'52fd3723-dd3f-43db-8506-be1bcc5603a7',39,NULL,NULL,NULL,NULL,NULL),(125,1071,'52fd3723-dd3f-43db-8506-be1bcc5603a7',44,NULL,NULL,NULL,NULL,NULL),(126,1071,'52fd3723-dd3f-43db-8506-be1bcc5603a7',40,0,NULL,NULL,NULL,NULL),(127,1071,'52fd3723-dd3f-43db-8506-be1bcc5603a7',45,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cmspropertydata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmspropertytype`
--

DROP TABLE IF EXISTS `cmspropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmspropertytype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dataTypeId` int(11) NOT NULL,
  `contentTypeId` int(11) NOT NULL,
  `propertyTypeGroupId` int(11) DEFAULT NULL,
  `Alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sortOrder` int(11) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `validationRegExp` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` varchar(2000) CHARACTER SET utf8 DEFAULT NULL,
  `UniqueID` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_cmsPropertyTypeUniqueID` (`UniqueID`),
  KEY `dataTypeId` (`dataTypeId`),
  KEY `contentTypeId` (`contentTypeId`),
  KEY `propertyTypeGroupId` (`propertyTypeGroupId`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmspropertytype`
--

LOCK TABLES `cmspropertytype` WRITE;
/*!40000 ALTER TABLE `cmspropertytype` DISABLE KEYS */;
INSERT INTO `cmspropertytype` VALUES (6,1043,1032,3,'umbracoFile','Upload image',0,0,NULL,NULL,'00000006-0000-0000-0000-000000000000'),(7,-92,1032,3,'umbracoWidth','Width',0,0,NULL,NULL,'00000007-0000-0000-0000-000000000000'),(8,-92,1032,3,'umbracoHeight','Height',0,0,NULL,NULL,'00000008-0000-0000-0000-000000000000'),(9,-92,1032,3,'umbracoBytes','Size',0,0,NULL,NULL,'00000009-0000-0000-0000-000000000000'),(10,-92,1032,3,'umbracoExtension','Type',0,0,NULL,NULL,'0000000a-0000-0000-0000-000000000000'),(24,-90,1033,4,'umbracoFile','Upload file',0,0,NULL,NULL,'00000018-0000-0000-0000-000000000000'),(25,-92,1033,4,'umbracoExtension','Type',0,0,NULL,NULL,'00000019-0000-0000-0000-000000000000'),(26,-92,1033,4,'umbracoBytes','Size',0,0,NULL,NULL,'0000001a-0000-0000-0000-000000000000'),(27,-96,1031,5,'contents','Contents:',0,0,NULL,NULL,'0000001b-0000-0000-0000-000000000000'),(28,-89,1044,11,'umbracoMemberComments','Comments',0,0,NULL,NULL,'0000001c-0000-0000-0000-000000000000'),(29,-92,1044,11,'umbracoMemberFailedPasswordAttempts','Failed Password Attempts',1,0,NULL,NULL,'0000001d-0000-0000-0000-000000000000'),(30,-49,1044,11,'umbracoMemberApproved','Is Approved',2,0,NULL,NULL,'0000001e-0000-0000-0000-000000000000'),(31,-49,1044,11,'umbracoMemberLockedOut','Is Locked Out',3,0,NULL,NULL,'0000001f-0000-0000-0000-000000000000'),(32,-92,1044,11,'umbracoMemberLastLockoutDate','Last Lockout Date',4,0,NULL,NULL,'00000020-0000-0000-0000-000000000000'),(33,-92,1044,11,'umbracoMemberLastLogin','Last Login Date',5,0,NULL,NULL,'00000021-0000-0000-0000-000000000000'),(34,-92,1044,11,'umbracoMemberLastPasswordChangeDate','Last Password Change Date',6,0,NULL,NULL,'00000022-0000-0000-0000-000000000000'),(35,-92,1044,NULL,'umbracoMemberPasswordRetrievalAnswer','Password Answer',0,0,NULL,NULL,'34814442-e81a-46e5-9aba-12ec521ff7c6'),(36,-92,1044,NULL,'umbracoMemberPasswordRetrievalQuestion','Password Question',1,0,NULL,NULL,'397f6e8e-d506-4c13-a749-29bc1069b7fd'),(37,-88,1052,12,'websiteTitle','Website Title',0,0,NULL,NULL,'c7ee696e-ece1-4cbb-ab97-8a35ebc56dcf'),(38,-89,1057,13,'umbracoMemberComments','Comments',0,0,NULL,NULL,'c084f4f2-11aa-4acf-89ec-1866238903ff'),(39,-92,1057,13,'umbracoMemberFailedPasswordAttempts','Failed Password Attempts',1,0,NULL,NULL,'a410d181-83f0-44ca-89aa-b171bff3de60'),(40,-49,1057,13,'umbracoMemberApproved','Is Approved',2,0,NULL,NULL,'72fe0007-8c92-4e21-ae37-8b07da9d366a'),(41,-49,1057,13,'umbracoMemberLockedOut','Is Locked Out',3,0,NULL,NULL,'5ff31abc-5f42-4c07-b561-64de06c62893'),(42,-92,1057,13,'umbracoMemberLastLockoutDate','Last Lockout Date',4,0,NULL,NULL,'9db59a9f-1e5a-4230-8f0d-448ec7d07c20'),(43,-92,1057,13,'umbracoMemberLastLogin','Last Login Date',5,0,NULL,NULL,'bfcd556b-37bc-4a3b-b188-5c5103a94cac'),(44,-92,1057,13,'umbracoMemberLastPasswordChangeDate','Last Password Change Date',6,0,NULL,NULL,'3878ba6f-a755-4780-a1dd-77cf56c3dfcb'),(45,-92,1057,13,'umbracoMemberPasswordRetrievalAnswer','Password Answer',7,0,NULL,NULL,'29a3ccc5-df23-4e2c-a645-2fbeef2bba1e'),(46,-92,1057,13,'umbracoMemberPasswordRetrievalQuestion','Password Question',8,0,NULL,NULL,'3502745e-923c-4384-b29a-7d0e193e47d2');
/*!40000 ALTER TABLE `cmspropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmspropertytypegroup`
--

DROP TABLE IF EXISTS `cmspropertytypegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmspropertytypegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contenttypeNodeId` int(11) NOT NULL,
  `text` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sortorder` int(11) NOT NULL,
  `uniqueID` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_cmsPropertyTypeGroupUniqueID` (`uniqueID`),
  KEY `contenttypeNodeId` (`contenttypeNodeId`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmspropertytypegroup`
--

LOCK TABLES `cmspropertytypegroup` WRITE;
/*!40000 ALTER TABLE `cmspropertytypegroup` DISABLE KEYS */;
INSERT INTO `cmspropertytypegroup` VALUES (3,1032,'Image',1,'79ed4d07-254a-42cf-8fa9-ebe1c116a596'),(4,1033,'File',1,'50899f9c-023a-4466-b623-aba9049885fe'),(5,1031,'Contents',1,'79995fa2-63ee-453c-a29b-2e66f324cdbe'),(11,1044,'Membership',1,'0756729d-d665-46e3-b84a-37aceaa614f8'),(12,1052,'Content',0,'95ce9c58-d707-4513-b1da-686d92e68ee8'),(13,1057,'Membership',0,'b99808c2-8ac5-45e2-951f-20856d6509c0');
/*!40000 ALTER TABLE `cmspropertytypegroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmstagrelationship`
--

DROP TABLE IF EXISTS `cmstagrelationship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmstagrelationship` (
  `nodeId` int(11) NOT NULL,
  `tagId` int(11) NOT NULL,
  `propertyTypeId` int(11) NOT NULL,
  PRIMARY KEY (`nodeId`,`propertyTypeId`,`tagId`),
  KEY `tagId` (`tagId`),
  KEY `propertyTypeId` (`propertyTypeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmstagrelationship`
--

LOCK TABLES `cmstagrelationship` WRITE;
/*!40000 ALTER TABLE `cmstagrelationship` DISABLE KEYS */;
/*!40000 ALTER TABLE `cmstagrelationship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmstags`
--

DROP TABLE IF EXISTS `cmstags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmstags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ParentId` int(11) DEFAULT NULL,
  `group` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_cmsTags` (`tag`,`group`),
  KEY `ParentId` (`ParentId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmstags`
--

LOCK TABLES `cmstags` WRITE;
/*!40000 ALTER TABLE `cmstags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cmstags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmstask`
--

DROP TABLE IF EXISTS `cmstask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmstask` (
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskTypeId` int(11) NOT NULL,
  `nodeId` int(11) NOT NULL,
  `parentUserId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `DateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Comment` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `taskTypeId` (`taskTypeId`),
  KEY `nodeId` (`nodeId`),
  KEY `parentUserId` (`parentUserId`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmstask`
--

LOCK TABLES `cmstask` WRITE;
/*!40000 ALTER TABLE `cmstask` DISABLE KEYS */;
/*!40000 ALTER TABLE `cmstask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmstasktype`
--

DROP TABLE IF EXISTS `cmstasktype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmstasktype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_cmsTaskType_alias` (`alias`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmstasktype`
--

LOCK TABLES `cmstasktype` WRITE;
/*!40000 ALTER TABLE `cmstasktype` DISABLE KEYS */;
INSERT INTO `cmstasktype` VALUES (1,'toTranslate');
/*!40000 ALTER TABLE `cmstasktype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmstemplate`
--

DROP TABLE IF EXISTS `cmstemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmstemplate` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `nodeId` int(11) NOT NULL,
  `alias` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `design` longtext NOT NULL,
  PRIMARY KEY (`pk`),
  KEY `IX_cmsTemplate_nodeId` (`nodeId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmstemplate`
--

LOCK TABLES `cmstemplate` WRITE;
/*!40000 ALTER TABLE `cmstemplate` DISABLE KEYS */;
INSERT INTO `cmstemplate` VALUES (1,1051,'Master','@inherits Umbraco.Web.Mvc.UmbracoTemplatePage\r\n@using ContentModels = Umbraco.Web.PublishedContentModels;\r\n@{\r\n	Layout = null;\r\n}\r\n\r\n<html>\r\n    <head>\r\n        <title>@Umbraco.Field(\"websiteTitle\")</title>\r\n    </head>\r\n    \r\n    <body>\r\n        <div class=\"header\">\r\n            \r\n        </div>\r\n        <div class=\"wrapper\">\r\n        @RenderBody()\r\n        </div>\r\n        <div class=\"footer\">\r\n            \r\n        </div>\r\n    </body>\r\n</html>'),(2,1053,'SellerRegistration','@inherits Umbraco.Web.Mvc.UmbracoTemplatePage\n@{\n	Layout = \"Master.cshtml\";\n}\n\n<div class=\"container\">\n    @{ Html.RenderAction(\"RenderForm\", \"SellerRegistrationSurface\"); }\n</div>\n\n');
/*!40000 ALTER TABLE `cmstemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracoaccess`
--

DROP TABLE IF EXISTS `umbracoaccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracoaccess` (
  `id` char(36) NOT NULL,
  `nodeId` int(11) NOT NULL,
  `loginNodeId` int(11) NOT NULL,
  `noAccessNodeId` int(11) NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IX_umbracoAccess_nodeId` (`nodeId`),
  KEY `loginNodeId` (`loginNodeId`),
  KEY `noAccessNodeId` (`noAccessNodeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracoaccess`
--

LOCK TABLES `umbracoaccess` WRITE;
/*!40000 ALTER TABLE `umbracoaccess` DISABLE KEYS */;
/*!40000 ALTER TABLE `umbracoaccess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracoaccessrule`
--

DROP TABLE IF EXISTS `umbracoaccessrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracoaccessrule` (
  `id` char(36) NOT NULL,
  `accessId` char(36) NOT NULL,
  `ruleValue` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ruleType` varchar(255) CHARACTER SET utf8 NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracoaccessrule`
--

LOCK TABLES `umbracoaccessrule` WRITE;
/*!40000 ALTER TABLE `umbracoaccessrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `umbracoaccessrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracocacheinstruction`
--

DROP TABLE IF EXISTS `umbracocacheinstruction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracocacheinstruction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `utcStamp` timestamp NOT NULL,
  `jsonInstruction` longtext NOT NULL,
  `originated` varchar(500) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracocacheinstruction`
--

LOCK TABLES `umbracocacheinstruction` WRITE;
/*!40000 ALTER TABLE `umbracocacheinstruction` DISABLE KEYS */;
INSERT INTO `umbracocacheinstruction` VALUES (1,'2017-09-05 05:33:09','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null},{\"RefreshType\":4,\"RefresherId\":\"6902e22c-9c10-483c-91f3-66b7cae9e2f5\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":null,\"JsonPayload\":\"[{\\\"Alias\\\":\\\"master\\\",\\\"Id\\\":1052,\\\"PropertyTypeIds\\\":[37],\\\"Type\\\":\\\"IContentType\\\",\\\"AliasChanged\\\":true,\\\"PropertyRemoved\\\":false,\\\"PropertyTypeAliasChanged\\\":false,\\\"DescendantPayloads\\\":[],\\\"WasDeleted\\\":false,\\\"IsNew\\\":true}]\"}]','PORTEGE//LM/W3SVC/2/ROOT [P644/D2] E8C1550257624457925D27F97B06B6C9'),(2,'2017-09-05 05:33:34','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P644/D2] E8C1550257624457925D27F97B06B6C9'),(3,'2017-09-05 05:33:49','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P644/D2] E8C1550257624457925D27F97B06B6C9'),(4,'2017-09-05 05:34:19','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P644/D2] E8C1550257624457925D27F97B06B6C9'),(5,'2017-09-05 05:37:50','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1053]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P644/D2] E8C1550257624457925D27F97B06B6C9'),(6,'2017-09-05 05:38:27','[{\"RefreshType\":4,\"RefresherId\":\"6902e22c-9c10-483c-91f3-66b7cae9e2f5\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":null,\"JsonPayload\":\"[{\\\"Alias\\\":\\\"sellerRegistration\\\",\\\"Id\\\":1054,\\\"PropertyTypeIds\\\":[],\\\"Type\\\":\\\"IContentType\\\",\\\"AliasChanged\\\":true,\\\"PropertyRemoved\\\":false,\\\"PropertyTypeAliasChanged\\\":false,\\\"DescendantPayloads\\\":[],\\\"WasDeleted\\\":false,\\\"IsNew\\\":true}]\"}]','PORTEGE//LM/W3SVC/2/ROOT [P644/D2] E8C1550257624457925D27F97B06B6C9'),(7,'2017-09-05 05:38:36','[{\"RefreshType\":4,\"RefresherId\":\"6902e22c-9c10-483c-91f3-66b7cae9e2f5\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":null,\"JsonPayload\":\"[{\\\"Alias\\\":\\\"sellerRegistration\\\",\\\"Id\\\":1054,\\\"PropertyTypeIds\\\":[],\\\"Type\\\":\\\"IContentType\\\",\\\"AliasChanged\\\":false,\\\"PropertyRemoved\\\":false,\\\"PropertyTypeAliasChanged\\\":false,\\\"DescendantPayloads\\\":[],\\\"WasDeleted\\\":false,\\\"IsNew\\\":false}]\"}]','PORTEGE//LM/W3SVC/2/ROOT [P644/D2] E8C1550257624457925D27F97B06B6C9'),(8,'2017-09-05 06:28:12','[{\"RefreshType\":4,\"RefresherId\":\"6902e22c-9c10-483c-91f3-66b7cae9e2f5\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":null,\"JsonPayload\":\"[{\\\"Alias\\\":\\\"master\\\",\\\"Id\\\":1052,\\\"PropertyTypeIds\\\":[37],\\\"Type\\\":\\\"IContentType\\\",\\\"AliasChanged\\\":false,\\\"PropertyRemoved\\\":false,\\\"PropertyTypeAliasChanged\\\":false,\\\"DescendantPayloads\\\":[],\\\"WasDeleted\\\":false,\\\"IsNew\\\":false}]\"}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D2] D4694D7D043841BDA3D140947F608494'),(9,'2017-09-05 06:28:29','[{\"RefreshType\":3,\"RefresherId\":\"27ab3022-3dfa-47b6-9119-5945bc88fd66\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1055]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D2] D4694D7D043841BDA3D140947F608494'),(10,'2017-09-05 06:29:06','[{\"RefreshType\":3,\"RefresherId\":\"27ab3022-3dfa-47b6-9119-5945bc88fd66\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1056]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D2] D4694D7D043841BDA3D140947F608494'),(11,'2017-09-05 06:38:35','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1053]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(12,'2017-09-05 06:39:18','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1053]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(13,'2017-09-05 06:39:48','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1053]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(14,'2017-09-05 06:39:53','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(15,'2017-09-05 06:40:25','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(16,'2017-09-05 06:40:32','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(17,'2017-09-05 06:41:00','[{\"RefreshType\":4,\"RefresherId\":\"6902e22c-9c10-483c-91f3-66b7cae9e2f5\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":null,\"JsonPayload\":\"[{\\\"Alias\\\":\\\"master\\\",\\\"Id\\\":1052,\\\"PropertyTypeIds\\\":[37],\\\"Type\\\":\\\"IContentType\\\",\\\"AliasChanged\\\":false,\\\"PropertyRemoved\\\":false,\\\"PropertyTypeAliasChanged\\\":false,\\\"DescendantPayloads\\\":[],\\\"WasDeleted\\\":false,\\\"IsNew\\\":false}]\"}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(18,'2017-09-05 06:41:54','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(19,'2017-09-05 06:54:42','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(20,'2017-09-05 06:54:42','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(21,'2017-09-05 06:54:42','[{\"RefreshType\":3,\"RefresherId\":\"dd12b6a0-14b9-46e8-8800-c154f74047c8\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1051]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D4] 335A8A7C97A040A7B444C3A1B9887A03'),(22,'2017-09-05 07:00:42','[{\"RefreshType\":4,\"RefresherId\":\"6902e22c-9c10-483c-91f3-66b7cae9e2f5\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":null,\"JsonPayload\":\"[{\\\"Alias\\\":\\\"seller\\\",\\\"Id\\\":1057,\\\"PropertyTypeIds\\\":[38,39,40,41,42,43,44,45,46],\\\"Type\\\":\\\"IMemberType\\\",\\\"AliasChanged\\\":true,\\\"PropertyRemoved\\\":false,\\\"PropertyTypeAliasChanged\\\":false,\\\"DescendantPayloads\\\":[],\\\"WasDeleted\\\":false,\\\"IsNew\\\":true}]\"}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D5] B86016FA5CCE4049BD12E222284DE0CC'),(23,'2017-09-05 07:04:12','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1058]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D6] B00AFD8B9DDC4D54BFD2288166E378DB'),(24,'2017-09-05 07:06:14','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1059]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D7] E7B745F8F5E54A87A5CE67CDF42085DD'),(25,'2017-09-05 07:08:07','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1060]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D9] 1DAF0E037C074EB597F0B95189CB6AD3'),(26,'2017-09-05 07:08:50','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1061]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D9] 1DAF0E037C074EB597F0B95189CB6AD3'),(27,'2017-09-05 07:09:07','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1062]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D9] 1DAF0E037C074EB597F0B95189CB6AD3'),(28,'2017-09-05 07:10:56','[{\"RefreshType\":5,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":1060,\"JsonIds\":null,\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D9] 1DAF0E037C074EB597F0B95189CB6AD3'),(29,'2017-09-05 07:12:42','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1063]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D11] 8E7DB4D9F41A46D290361383C43A0C7A'),(30,'2017-09-05 07:13:24','[{\"RefreshType\":5,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":1063,\"JsonIds\":null,\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D11] 8E7DB4D9F41A46D290361383C43A0C7A'),(31,'2017-09-05 07:13:27','[{\"RefreshType\":5,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":1061,\"JsonIds\":null,\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D11] 8E7DB4D9F41A46D290361383C43A0C7A'),(32,'2017-09-05 07:13:31','[{\"RefreshType\":5,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":1062,\"JsonIds\":null,\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D11] 8E7DB4D9F41A46D290361383C43A0C7A'),(33,'2017-09-05 07:13:34','[{\"RefreshType\":5,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":1059,\"JsonIds\":null,\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D11] 8E7DB4D9F41A46D290361383C43A0C7A'),(34,'2017-09-05 07:13:37','[{\"RefreshType\":5,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":1058,\"JsonIds\":null,\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D11] 8E7DB4D9F41A46D290361383C43A0C7A'),(35,'2017-09-05 07:20:05','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1064]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D12] 0551FBA772ED42CFBF704E66917EA11F'),(36,'2017-09-05 07:21:42','[{\"RefreshType\":5,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":1064,\"JsonIds\":null,\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D13] 9D7C8F0FA8F84A99A8C77DA4B80ABC08'),(37,'2017-09-05 07:21:56','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1065]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D13] 9D7C8F0FA8F84A99A8C77DA4B80ABC08'),(38,'2017-09-05 07:22:24','[{\"RefreshType\":5,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":1065,\"JsonIds\":null,\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D13] 9D7C8F0FA8F84A99A8C77DA4B80ABC08'),(39,'2017-09-05 07:22:32','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1066]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D13] 9D7C8F0FA8F84A99A8C77DA4B80ABC08'),(40,'2017-09-05 07:22:46','[{\"RefreshType\":5,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":1066,\"JsonIds\":null,\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D13] 9D7C8F0FA8F84A99A8C77DA4B80ABC08'),(41,'2017-09-05 07:23:17','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1067]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D14] 2BD44C91AF464356883DAD0BC73CBAAC'),(42,'2017-09-05 07:23:54','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1068]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D14] 2BD44C91AF464356883DAD0BC73CBAAC'),(43,'2017-09-05 07:24:10','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1069]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D14] 2BD44C91AF464356883DAD0BC73CBAAC'),(44,'2017-09-05 07:24:23','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1070]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D14] 2BD44C91AF464356883DAD0BC73CBAAC'),(45,'2017-09-05 07:24:38','[{\"RefreshType\":3,\"RefresherId\":\"e285df34-acdc-4226-ae32-c0cb5cf388da\",\"GuidId\":\"00000000-0000-0000-0000-000000000000\",\"IntId\":0,\"JsonIds\":\"[1071]\",\"JsonPayload\":null}]','PORTEGE//LM/W3SVC/2/ROOT [P9720/D14] 2BD44C91AF464356883DAD0BC73CBAAC');
/*!40000 ALTER TABLE `umbracocacheinstruction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracodomains`
--

DROP TABLE IF EXISTS `umbracodomains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracodomains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domainDefaultLanguage` int(11) DEFAULT NULL,
  `domainRootStructureID` int(11) DEFAULT NULL,
  `domainName` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `domainRootStructureID` (`domainRootStructureID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracodomains`
--

LOCK TABLES `umbracodomains` WRITE;
/*!40000 ALTER TABLE `umbracodomains` DISABLE KEYS */;
/*!40000 ALTER TABLE `umbracodomains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracoexternallogin`
--

DROP TABLE IF EXISTS `umbracoexternallogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracoexternallogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `loginProvider` varchar(4000) CHARACTER SET utf8 NOT NULL,
  `providerKey` varchar(4000) CHARACTER SET utf8 NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracoexternallogin`
--

LOCK TABLES `umbracoexternallogin` WRITE;
/*!40000 ALTER TABLE `umbracoexternallogin` DISABLE KEYS */;
/*!40000 ALTER TABLE `umbracoexternallogin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracolanguage`
--

DROP TABLE IF EXISTS `umbracolanguage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracolanguage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `languageISOCode` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `languageCultureName` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_umbracoLanguage_languageISOCode` (`languageISOCode`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracolanguage`
--

LOCK TABLES `umbracolanguage` WRITE;
/*!40000 ALTER TABLE `umbracolanguage` DISABLE KEYS */;
INSERT INTO `umbracolanguage` VALUES (1,'en-US','en-US');
/*!40000 ALTER TABLE `umbracolanguage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracolock`
--

DROP TABLE IF EXISTS `umbracolock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracolock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracolock`
--

LOCK TABLES `umbracolock` WRITE;
/*!40000 ALTER TABLE `umbracolock` DISABLE KEYS */;
INSERT INTO `umbracolock` VALUES (-331,1,'Servers');
/*!40000 ALTER TABLE `umbracolock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracolog`
--

DROP TABLE IF EXISTS `umbracolog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracolog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `NodeId` int(11) NOT NULL,
  `Datestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logHeader` varchar(50) CHARACTER SET utf8 NOT NULL,
  `logComment` varchar(4000) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_umbracoLog` (`NodeId`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracolog`
--

LOCK TABLES `umbracolog` WRITE;
/*!40000 ALTER TABLE `umbracolog` DISABLE KEYS */;
INSERT INTO `umbracolog` VALUES (1,0,0,'2017-09-05 13:33:09','Save','Save Template performed by user'),(2,0,1052,'2017-09-05 13:33:09','Save','Save ContentType performed by user'),(3,0,1051,'2017-09-05 13:33:34','Save','Save Template performed by user'),(4,0,1051,'2017-09-05 13:33:49','Save','Save Template performed by user'),(5,0,1051,'2017-09-05 13:34:19','Save','Save Template performed by user'),(6,0,0,'2017-09-05 13:37:50','Save','Save Template performed by user'),(7,0,1054,'2017-09-05 13:38:27','Save','Save ContentType performed by user'),(8,0,1054,'2017-09-05 13:38:36','Save','Save ContentType performed by user'),(9,0,0,'2017-09-05 14:27:58','New','Content \'\' was created'),(10,0,1052,'2017-09-05 14:28:12','Save','Save ContentType performed by user'),(11,0,0,'2017-09-05 14:28:18','New','Content \'\' was created'),(12,0,0,'2017-09-05 14:28:26','Publish','Save and Publish performed by user'),(13,0,0,'2017-09-05 14:28:56','New','Content \'\' was created'),(14,0,0,'2017-09-05 14:29:06','Publish','Save and Publish performed by user'),(15,0,1053,'2017-09-05 14:38:35','Save','Save Template performed by user'),(16,0,1053,'2017-09-05 14:39:18','Save','Save Template performed by user'),(17,0,1053,'2017-09-05 14:39:48','Save','Save Template performed by user'),(18,0,1051,'2017-09-05 14:39:53','Save','Save Template performed by user'),(19,0,1051,'2017-09-05 14:40:25','Save','Save Template performed by user'),(20,0,1051,'2017-09-05 14:40:32','Save','Save Template performed by user'),(21,0,1052,'2017-09-05 14:41:00','Save','Save ContentType performed by user'),(22,0,1051,'2017-09-05 14:41:54','Save','Save Template performed by user'),(23,0,1051,'2017-09-05 14:54:42','Save','Save Template performed by user'),(24,0,1051,'2017-09-05 14:54:42','Save','Save Template performed by user'),(25,0,1051,'2017-09-05 14:54:42','Save','Save Template performed by user');
/*!40000 ALTER TABLE `umbracolog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracomigration`
--

DROP TABLE IF EXISTS `umbracomigration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracomigration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_umbracoMigration` (`name`,`version`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracomigration`
--

LOCK TABLES `umbracomigration` WRITE;
/*!40000 ALTER TABLE `umbracomigration` DISABLE KEYS */;
INSERT INTO `umbracomigration` VALUES (1,'Umbraco','2017-09-04 03:02:28','7.6.5');
/*!40000 ALTER TABLE `umbracomigration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbraconode`
--

DROP TABLE IF EXISTS `umbraconode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbraconode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trashed` tinyint(1) NOT NULL DEFAULT '0',
  `parentID` int(11) NOT NULL,
  `nodeUser` int(11) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `path` varchar(150) CHARACTER SET utf8 NOT NULL,
  `sortOrder` int(11) NOT NULL,
  `uniqueID` char(36) NOT NULL,
  `text` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `nodeObjectType` char(36) DEFAULT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IX_umbracoNodeTrashed` (`trashed`),
  KEY `IX_umbracoNodeParentId` (`parentID`),
  KEY `IX_umbracoNodePath` (`path`),
  KEY `IX_umbracoNodeUniqueID` (`uniqueID`),
  KEY `IX_umbracoNodeObjectType` (`nodeObjectType`)
) ENGINE=MyISAM AUTO_INCREMENT=1072 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbraconode`
--

LOCK TABLES `umbraconode` WRITE;
/*!40000 ALTER TABLE `umbraconode` DISABLE KEYS */;
INSERT INTO `umbraconode` VALUES (-1,0,-1,0,0,'-1',0,'916724a5-173d-4619-b97e-b9de133dd6f5','SYSTEM DATA: umbraco master root','ea7d8624-4cfe-4578-a871-24aa946bf34d','2017-09-04 03:01:56'),(-20,0,-1,0,0,'-1,-20',0,'0f582a79-1e41-4cf0-bfa0-76340651891a','Recycle Bin','01bb7ff2-24dc-4c0c-95a2-c24ef72bbac8','2017-09-04 03:01:56'),(-21,0,-1,0,0,'-1,-21',0,'bf7c7cbc-952f-4518-97a2-69e9c7b33842','Recycle Bin','cf3d8e34-1c1c-41e9-ae56-878b57b32113','2017-09-04 03:01:56'),(-92,0,-1,0,1,'-1,-92',35,'f0bc4bfb-b499-40d6-ba86-058885a5178c','Label','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-90,0,-1,0,1,'-1,-90',34,'84c6b441-31df-4ffe-b67e-67d5bc3ae65a','Upload','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-89,0,-1,0,1,'-1,-89',33,'c6bac0dd-4ab9-45b1-8e30-e4b619ee5da3','Textarea','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-88,0,-1,0,1,'-1,-88',32,'0cc0eba1-9960-42c9-bf9b-60e150b429ae','Textstring','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-87,0,-1,0,1,'-1,-87',4,'ca90c950-0aff-4e72-b976-a30b1ac57dad','Richtext editor','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-51,0,-1,0,1,'-1,-51',2,'2e6d3631-066e-44b8-aec4-96f09099b2b5','Numeric','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-49,0,-1,0,1,'-1,-49',2,'92897bc6-a5f3-4ffe-ae27-f2e7e33dda49','True/false','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-43,0,-1,0,1,'-1,-43',2,'fbaf13a8-4036-41f2-93a3-974f678c312a','Checkbox list','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-42,0,-1,0,1,'-1,-42',2,'0b6a45e7-44ba-430d-9da5-4e46060b9e03','Dropdown','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-41,0,-1,0,1,'-1,-41',2,'5046194e-4237-453c-a547-15db3a07c4e1','Date Picker','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-40,0,-1,0,1,'-1,-40',2,'bb5f57c9-ce2b-4bb9-b697-4caca783a805','Radiobox','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-39,0,-1,0,1,'-1,-39',2,'f38f0ac7-1d27-439c-9f3f-089cd8825a53','Dropdown multiple','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-37,0,-1,0,1,'-1,-37',2,'0225af17-b302-49cb-9176-b9f35cab9c17','Approved Color','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-36,0,-1,0,1,'-1,-36',2,'e4d66c0f-b935-4200-81f0-025f7256b89a','Date Picker with time','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-95,0,-1,0,1,'-1,-95',2,'c0808dd3-8133-4e4b-8ce8-e2bea84a96a4','List View - Content','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-96,0,-1,0,1,'-1,-96',2,'3a0156c4-3b8c-4803-bdc1-6871faa83fff','List View - Media','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(-97,0,-1,0,1,'-1,-97',2,'aa2c52a0-ce87-4e65-a47c-7df09358585d','List View - Members','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(1031,0,-1,0,1,'-1,1031',2,'f38bd2d7-65d0-48e6-95dc-87ce06ec2d3d','Folder','4ea4382b-2f5a-4c2b-9587-ae9b3cf3602e','2017-09-04 03:01:56'),(1032,0,-1,0,1,'-1,1032',2,'cc07b313-0843-4aa8-bbda-871c8da728c8','Image','4ea4382b-2f5a-4c2b-9587-ae9b3cf3602e','2017-09-04 03:01:56'),(1033,0,-1,0,1,'-1,1033',2,'4c52d8ab-54e6-40cd-999c-7a5f24903e4d','File','4ea4382b-2f5a-4c2b-9587-ae9b3cf3602e','2017-09-04 03:01:56'),(1041,0,-1,0,1,'-1,1041',2,'b6b73142-b9c1-4bf8-a16d-e1c23320b549','Tags','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(1043,0,-1,0,1,'-1,1043',2,'1df9f033-e6d4-451f-b8d2-e0cbc50a836f','Image Cropper','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(1044,0,-1,0,1,'-1,1044',0,'d59be02f-1df9-4228-aa1e-01917d806cda','Member','9b5416fb-e72f-45a9-a07b-5a9a2709ce43','2017-09-04 03:01:56'),(1046,0,-1,0,1,'-1,1046',2,'fd1e0da5-5606-4862-b679-5d0cf3a52a59','Content Picker','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(1047,0,-1,0,1,'-1,1047',2,'1ea2e01f-ebd8-4ce1-8d71-6b1149e63548','Member Picker','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(1048,0,-1,0,1,'-1,1048',2,'135d60e0-64d9-49ed-ab08-893c9ba44ae5','Media Picker','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(1049,0,-1,0,1,'-1,1049',2,'9dbbcbbb-2327-434a-b355-af1b84e5010a','Multiple Media Picker','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(1050,0,-1,0,1,'-1,1050',2,'b4e3535a-1753-47e2-8568-602cf8cfee6f','Related Links','30a2a501-1978-4ddb-a57b-f7efed43ba3c','2017-09-04 03:01:56'),(1051,0,-1,NULL,1,'-1,1051',0,'a5393206-c4f3-4029-be35-e51f56bf0fc5','Master','6fbde604-4178-42ce-a10b-8a2600a2f07d','2017-09-05 13:33:09'),(1052,0,-1,0,1,'-1,1052',4,'819676a4-7df1-488a-845c-fc383fe2d9ac','Master','a2cb7800-f571-4787-9638-bc48539a0efb','2017-09-05 13:33:09'),(1053,0,1051,NULL,1,'-1,1051,1053',0,'f3b52630-4563-495d-a5c4-9f2bc3eae514','SellerRegistration','6fbde604-4178-42ce-a10b-8a2600a2f07d','2017-09-05 13:37:50'),(1054,0,-1,0,1,'-1,1054',2,'82d9fe26-41da-4e13-bef0-a981a86dc4f7','SellerRegistration','a2cb7800-f571-4787-9638-bc48539a0efb','2017-09-05 13:38:27'),(1055,0,-1,0,1,'-1,1055',0,'c3123efd-8013-403e-b413-a2ae2d4a48a1','Homepage','c66ba18e-eaf3-4cff-8a22-41b16d66a972','2017-09-05 14:28:26'),(1056,0,1055,0,2,'-1,1055,1056',0,'9b129fca-3d25-44b0-a828-f905a815d77c','Register as Seller','c66ba18e-eaf3-4cff-8a22-41b16d66a972','2017-09-05 14:29:06'),(1057,0,-1,0,1,'-1,1057',1,'12b7ba66-a625-4995-a266-dea9c94fc4c6','Seller','9b5416fb-e72f-45a9-a07b-5a9a2709ce43','2017-09-05 15:00:42'),(1067,0,-1,0,1,'-1,1067',0,'38d63467-bfa9-44a9-a02b-28de033378e5','Justin Ravago','39eb0f98-b348-42a1-8662-e7eb18487560','2017-09-05 15:23:09'),(1068,0,-1,0,1,'-1,1068',1,'13cdf5e1-5eb2-424a-b2c5-f4afd927aa2f','Frank Claveria','39eb0f98-b348-42a1-8662-e7eb18487560','2017-09-05 15:23:48'),(1069,0,-1,0,1,'-1,1069',2,'4eae977e-033e-4d53-9e09-b1d60d576e1e','Duane Gavino','39eb0f98-b348-42a1-8662-e7eb18487560','2017-09-05 15:24:05'),(1070,0,-1,0,1,'-1,1070',3,'5e2cc6d3-99d3-4f60-8f10-bea17f5a8d40','Danilo Buhain','39eb0f98-b348-42a1-8662-e7eb18487560','2017-09-05 15:24:18'),(1071,0,-1,0,1,'-1,1071',4,'3b8654b5-df96-47e4-8384-dfdddd4d4d5b','Christian Bailon','39eb0f98-b348-42a1-8662-e7eb18487560','2017-09-05 15:24:32');
/*!40000 ALTER TABLE `umbraconode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracoredirecturl`
--

DROP TABLE IF EXISTS `umbracoredirecturl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracoredirecturl` (
  `id` char(36) NOT NULL,
  `contentKey` char(36) NOT NULL,
  `createDateUtc` timestamp NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `urlHash` varchar(40) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_umbracoRedirectUrl` (`urlHash`,`contentKey`,`createDateUtc`),
  KEY `contentKey` (`contentKey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracoredirecturl`
--

LOCK TABLES `umbracoredirecturl` WRITE;
/*!40000 ALTER TABLE `umbracoredirecturl` DISABLE KEYS */;
/*!40000 ALTER TABLE `umbracoredirecturl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracorelation`
--

DROP TABLE IF EXISTS `umbracorelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracorelation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) NOT NULL,
  `childId` int(11) NOT NULL,
  `relType` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment` varchar(1000) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_umbracoRelation_parentChildType` (`parentId`,`childId`,`relType`),
  KEY `childId` (`childId`),
  KEY `relType` (`relType`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracorelation`
--

LOCK TABLES `umbracorelation` WRITE;
/*!40000 ALTER TABLE `umbracorelation` DISABLE KEYS */;
/*!40000 ALTER TABLE `umbracorelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracorelationtype`
--

DROP TABLE IF EXISTS `umbracorelationtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracorelationtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeUniqueId` char(36) NOT NULL,
  `dual` tinyint(1) NOT NULL,
  `parentObjectType` char(36) NOT NULL,
  `childObjectType` char(36) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `alias` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_umbracoRelationType_UniqueId` (`typeUniqueId`),
  KEY `IX_umbracoRelationType_name` (`name`),
  KEY `IX_umbracoRelationType_alias` (`alias`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracorelationtype`
--

LOCK TABLES `umbracorelationtype` WRITE;
/*!40000 ALTER TABLE `umbracorelationtype` DISABLE KEYS */;
INSERT INTO `umbracorelationtype` VALUES (1,'4cbeb612-e689-3563-b755-bf3ede295433',1,'c66ba18e-eaf3-4cff-8a22-41b16d66a972','c66ba18e-eaf3-4cff-8a22-41b16d66a972','Relate Document On Copy','relateDocumentOnCopy'),(2,'0cc3507c-66ab-3091-8913-3d998148e423',0,'c66ba18e-eaf3-4cff-8a22-41b16d66a972','c66ba18e-eaf3-4cff-8a22-41b16d66a972','Relate Parent Document On Delete','relateParentDocumentOnDelete');
/*!40000 ALTER TABLE `umbracorelationtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracoserver`
--

DROP TABLE IF EXISTS `umbracoserver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracoserver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(500) CHARACTER SET utf8 NOT NULL,
  `computerName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `registeredDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastNotifiedDate` timestamp NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `isMaster` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_computerName` (`computerName`),
  KEY `IX_umbracoServer_isActive` (`isActive`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracoserver`
--

LOCK TABLES `umbracoserver` WRITE;
/*!40000 ALTER TABLE `umbracoserver` DISABLE KEYS */;
INSERT INTO `umbracoserver` VALUES (1,'http://localhost:56928/umbraco','PORTEGE//LM/W3SVC/2/ROOT','2017-09-04 03:02:51','2017-09-05 15:25:21',1,1);
/*!40000 ALTER TABLE `umbracoserver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracouser`
--

DROP TABLE IF EXISTS `umbracouser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracouser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userDisabled` tinyint(1) NOT NULL DEFAULT '0',
  `userNoConsole` tinyint(1) NOT NULL DEFAULT '0',
  `userType` int(11) NOT NULL,
  `startStructureID` int(11) NOT NULL,
  `startMediaID` int(11) DEFAULT NULL,
  `userName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `userLogin` varchar(125) CHARACTER SET utf8 NOT NULL,
  `userPassword` varchar(500) CHARACTER SET utf8 NOT NULL,
  `userEmail` varchar(255) CHARACTER SET utf8 NOT NULL,
  `userLanguage` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `securityStampToken` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `failedLoginAttempts` int(11) DEFAULT NULL,
  `lastLockoutDate` timestamp NULL DEFAULT NULL,
  `lastPasswordChangeDate` timestamp NULL DEFAULT NULL,
  `lastLoginDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_umbracoUser_userLogin` (`userLogin`),
  KEY `userType` (`userType`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracouser`
--

LOCK TABLES `umbracouser` WRITE;
/*!40000 ALTER TABLE `umbracouser` DISABLE KEYS */;
INSERT INTO `umbracouser` VALUES (0,0,0,1,-1,-1,'Justin R','justin.ravago@redcoresolutions.com','uGa0qg4BpDap4sFfHgIqmw==k+6OuUAvz86XkAjpc60UL9smjw8zrojhqOPDeOKsGiI=','justin.ravago@redcoresolutions.com','en-GB','91e552aa-d7a7-4d5a-956f-8a75c76dfb67',NULL,NULL,'2017-09-04 03:02:28','2017-09-05 14:26:44');
/*!40000 ALTER TABLE `umbracouser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracouser2app`
--

DROP TABLE IF EXISTS `umbracouser2app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracouser2app` (
  `user` int(11) NOT NULL,
  `app` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`user`,`app`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracouser2app`
--

LOCK TABLES `umbracouser2app` WRITE;
/*!40000 ALTER TABLE `umbracouser2app` DISABLE KEYS */;
INSERT INTO `umbracouser2app` VALUES (0,'content'),(0,'developer'),(0,'forms'),(0,'media'),(0,'member'),(0,'settings'),(0,'users');
/*!40000 ALTER TABLE `umbracouser2app` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracouser2nodenotify`
--

DROP TABLE IF EXISTS `umbracouser2nodenotify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracouser2nodenotify` (
  `userId` int(11) NOT NULL,
  `nodeId` int(11) NOT NULL,
  `action` char(1) NOT NULL,
  PRIMARY KEY (`userId`,`nodeId`,`action`),
  KEY `nodeId` (`nodeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracouser2nodenotify`
--

LOCK TABLES `umbracouser2nodenotify` WRITE;
/*!40000 ALTER TABLE `umbracouser2nodenotify` DISABLE KEYS */;
/*!40000 ALTER TABLE `umbracouser2nodenotify` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracouser2nodepermission`
--

DROP TABLE IF EXISTS `umbracouser2nodepermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracouser2nodepermission` (
  `userId` int(11) NOT NULL,
  `nodeId` int(11) NOT NULL,
  `permission` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`userId`,`nodeId`,`permission`),
  KEY `IX_umbracoUser2NodePermission_nodeId` (`nodeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracouser2nodepermission`
--

LOCK TABLES `umbracouser2nodepermission` WRITE;
/*!40000 ALTER TABLE `umbracouser2nodepermission` DISABLE KEYS */;
/*!40000 ALTER TABLE `umbracouser2nodepermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umbracousertype`
--

DROP TABLE IF EXISTS `umbracousertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbracousertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userTypeAlias` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `userTypeName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `userTypeDefaultPermissions` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umbracousertype`
--

LOCK TABLES `umbracousertype` WRITE;
/*!40000 ALTER TABLE `umbracousertype` DISABLE KEYS */;
INSERT INTO `umbracousertype` VALUES (1,'admin','Administrators','CADMOSKTPIURZ:5F7'),(2,'writer','Writer','CAH:F'),(3,'editor','Editors','CADMOSKTPUZ:5F'),(4,'translator','Translator','AF');
/*!40000 ALTER TABLE `umbracousertype` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-05 23:25:50
